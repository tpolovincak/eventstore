import { DocumentClient } from 'aws-sdk/clients/dynamodb'
import { serializer as _serializer } from './serializer'

export interface Event<D extends object> {
  eventType: string
  data: D
  metadata?: object
}

export interface EventRecord extends Event<any> {
  eventId: string
  eventVersion: number
}

export interface Commit {
  aggregateId: string
  commitId: string
  commitVersion: number
  eventVersion: number
  events: EventRecord[]
  timestamp: any
}

export type LoadResult = [Event<any>[], number, number]

export interface IEventStoreConnection {
  load(aggregateId: string): Promise<LoadResult>
  save(aggregateId: string, events: Event<any>[], commitVersion?: number, eventVersion?: number): Promise<any>
}

const { query, put } = new DocumentClient()

const _load = (aggregateId: string, name: string) =>
  query({
    TableName: name,
    ConsistentRead: true,
    KeyConditionExpression: 'aggregateId = :a',
    ExpressionAttributeValues: {
      ':a': aggregateId,
    },
  }).promise()

const _save = (commit: any, name: string) =>
  put({
    TableName: name,
    Item: commit,
    ConditionExpression: 'attribute_not_exists(version)',
    ReturnValues: 'NONE',
  }).promise()

export const createConnection = (name: string): IEventStoreConnection => {
  const { serialize, deserialize } = _serializer

  return {
    load: async (aggregateId: string) => {
      return _load(aggregateId, name).then(({ Items }) => deserialize(Items))
    },
    save: async (aggregateId: string, events: Event<any>[], commitVersion = 0, eventVersion = 0) => {
      return _save(serialize(aggregateId, events, commitVersion, eventVersion), name)
    },
  }
}
