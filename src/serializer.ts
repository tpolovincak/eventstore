import * as uuid from 'uuid/v4'
import { Event, LoadResult, Commit, EventRecord } from './store'

export interface ISerializer {
  serialize(aggregateId: string, events: Event<any>[], commitVersion: number, eventVersion: number): Commit
  deserialize(commit: any): LoadResult
}

const _serialize = (aggregateId: string, events: Event<any>[], commitVersion: number, eventVersion: number): Commit => {
  return {
    aggregateId,
    commitId: uuid(),
    commitVersion: commitVersion + 1,
    eventVersion: eventVersion + events.length,
    timestamp: Date.now(),
    events: events.map(({ eventType, data, metadata }, index) => ({
      eventId: uuid(),
      eventVersion: eventVersion + index + 1,
      eventType,
      data,
      metadata,
    })),
  }
}

const _deserialize = (commits: Commit[]) => {
  return commits.reduce<LoadResult>(
    ([events], commit) => [
      events.concat(
        commit.events.map(({ eventType, eventVersion, data, metadata }) => {
          return { eventType, data, metadata }
        })
      ),
      commit.commitVersion,
      commit.events.slice(-1)[0].eventVersion,
    ],
    [[], 0, 0]
  )
}

export const serializer: ISerializer = {
  serialize: _serialize,
  deserialize: _deserialize,
}
