"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid = require("uuid/v4");
const _serialize = (aggregateId, events, commitVersion, eventVersion) => {
    return {
        aggregateId,
        commitId: uuid(),
        commitVersion: commitVersion + 1,
        eventVersion: eventVersion + events.length,
        timestamp: Date.now(),
        events: events.map(({ eventType, data, metadata }, index) => ({
            eventId: uuid(),
            eventVersion: eventVersion + index + 1,
            eventType,
            data,
            metadata,
        })),
    };
};
const _deserialize = (commits) => {
    return commits.reduce(([events], commit) => [
        events.concat(commit.events.map(({ eventType, eventVersion, data, metadata }) => {
            return { eventType, data, metadata };
        })),
        commit.commitVersion,
        commit.events.slice(-1)[0].eventVersion,
    ], [[], 0, 0]);
};
exports.serializer = {
    serialize: _serialize,
    deserialize: _deserialize,
};
