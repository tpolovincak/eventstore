export interface Event<D extends object> {
    eventType: string;
    data: D;
    metadata?: object;
}
export interface EventRecord extends Event<any> {
    eventId: string;
    eventVersion: number;
}
export interface Commit {
    aggregateId: string;
    commitId: string;
    commitVersion: number;
    eventVersion: number;
    events: EventRecord[];
    timestamp: any;
}
export declare type LoadResult = [Event<any>[], number, number];
export interface IEventStoreConnection {
    load(aggregateId: string): Promise<LoadResult>;
    save(aggregateId: string, events: Event<any>[], commitVersion?: number, eventVersion?: number): Promise<any>;
}
export declare const createConnection: (name: string) => IEventStoreConnection;
