import { Event, LoadResult, Commit } from './store';
export interface ISerializer {
    serialize(aggregateId: string, events: Event<any>[], commitVersion: number, eventVersion: number): Commit;
    deserialize(commit: any): LoadResult;
}
export declare const serializer: ISerializer;
