"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const dynamodb_1 = require("aws-sdk/clients/dynamodb");
const serializer_1 = require("./serializer");
const { query, put } = new dynamodb_1.DocumentClient();
const _load = (aggregateId, name) => query({
    TableName: name,
    ConsistentRead: true,
    KeyConditionExpression: 'aggregateId = :a',
    ExpressionAttributeValues: {
        ':a': aggregateId,
    },
}).promise();
const _save = (commit, name) => put({
    TableName: name,
    Item: commit,
    ConditionExpression: 'attribute_not_exists(version)',
    ReturnValues: 'NONE',
}).promise();
exports.createConnection = (name) => {
    const { serialize, deserialize } = serializer_1.serializer;
    return {
        load: (aggregateId) => __awaiter(this, void 0, void 0, function* () {
            return _load(aggregateId, name).then(({ Items }) => deserialize(Items));
        }),
        save: (aggregateId, events, commitVersion = 0, eventVersion = 0) => __awaiter(this, void 0, void 0, function* () {
            return _save(serialize(aggregateId, events, commitVersion, eventVersion), name);
        }),
    };
};
